import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { LoginComponent } from './login/login.component';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import { RegisterComponent } from './register/register.component';
// import {FormBuilder, Validators} from '@angular/forms';
import {MatStepperModule} from '@angular/material/stepper';
import {  ReactiveFormsModule } from '@angular/forms';
import { IndexComponent } from './index/index.component';
import { Register1Component } from './register1/register1.component';
import { AboutComponent } from './about/about.component';
import { PresidentComponent } from './president/president.component';
import { ServiceComponent } from './service/service.component';
import { ContactComponent } from './contact/contact.component';
import { HeaderComponent } from './header/header.component';
import {AccordionModule} from 'primeng/accordion';    
import {MenuItem} from 'primeng/api';
import { PraticeComponent } from './pratice/pratice.component';       
import {CarouselModule} from 'primeng/carousel';
import {ButtonModule} from 'primeng/button';
import {ToastModule} from 'primeng/toast';
import { Header1Component } from './header1/header1.component';
import { HomeComponent } from './home/home.component';
import { NewmatchesComponent } from './newmatches/newmatches.component';
import { MyprofileComponent } from './myprofile/myprofile.component';
import { HttpClientModule } from '@angular/common/http';
import {MatPaginatorModule} from '@angular/material/paginator';
import { ToastrModule } from 'ngx-toastr';

import {NgxPhotoEditorModule} from "ngx-photo-editor";
import { NgImageSliderModule } from 'ng-image-slider';
import {MatTabsModule} from '@angular/material/tabs';

import { ViewprofileComponent } from './viewprofile/viewprofile.component';
import {MatRadioModule} from '@angular/material/radio';
import { ExtraComponent } from './extra/extra.component';

import {TabViewModule} from 'primeng/tabview';



// import { LoginService } from './login.service';

// import { ProductService } from './practice.service'
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    IndexComponent,
    Register1Component,
    AboutComponent,
    PresidentComponent,
    ServiceComponent,
    ContactComponent,
    HeaderComponent,
    PraticeComponent,
    Header1Component,
    HomeComponent,
    NewmatchesComponent,
    MyprofileComponent,
    ViewprofileComponent,
    ExtraComponent,
    
    

    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
   BrowserAnimationsModule,
  //  FlexLayoutModule,
   FormsModule,
   MatToolbarModule,
   MatInputModule,
   MatCardModule,
   MatMenuModule,
   MatIconModule,
   MatButtonModule,
   MatTableModule,
   MatSlideToggleModule,
   MatSelectModule,
   MatOptionModule,
   MatDatepickerModule,
   MatNativeDateModule ,
   MatRadioModule,
  //  FormBuilder,Validators
  MatStepperModule,
  ReactiveFormsModule,
  AccordionModule,
  CarouselModule,ButtonModule,ToastModule,
 HttpClientModule ,
 MatPaginatorModule,
 NgImageSliderModule,
 MatTabsModule,
 ToastrModule,
 NgxPhotoEditorModule,
 TabViewModule,
 
 ToastrModule.forRoot({
  closeButton: true,
  timeOut: 15000, // 15 seconds
  progressBar: true,
}),
  
  ],
  providers: [MatDatepickerModule ],
  bootstrap: [AppComponent]
})
export class AppModule { }
