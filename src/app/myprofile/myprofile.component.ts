import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.css']
})
export class MyprofileComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  imgCollection: Array<object> = [
    {
      image: "../../assets/m5.jpg",
      thumbImage: "../../assets/m5.jpg",
      alt: 'Image 1',
      title: 'Image 1'
    }, {
      image: "../../assets/m5.jpg",
      thumbImage: "../../assets/m5.jpg",
      title: 'Image 2',
      alt: 'Image 2'
    }, {
      image: "../../assets/m5.jpg",
      thumbImage: "../../assets/m5.jpg",
      title: 'Image 3',
      alt: 'Image 3'
    }, {
      image: "../../assets/m5.jpg",
      thumbImage: "../../assets/m5.jpg",
      title: 'Image 4',
      alt: 'Image 4'
    }, 
];


}
