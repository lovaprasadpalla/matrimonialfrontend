import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { observable } from 'rxjs'; 

@Injectable({
  providedIn: 'root'
})

export class LoginService {
   
   
  constructor(private http: HttpClient) { }
  userlogin(reqdata:any) {
    return this.http.post(`http://localhost:5000/Profilelogin`, reqdata);
  }
}


