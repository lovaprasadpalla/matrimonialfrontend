import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  imag:string='././'
  products:any=[];
  responsiveOptions :any =[]
  constructor() { this.responsiveOptions =[
    {
      breakpoint: '1024px',
      numVisible: 3,
      numScroll: 3
      
  },
  {
      breakpoint: '768px',
      numVisible: 2,
      numScroll: 2
  },
  {
      breakpoint: '560px',
      numVisible: 1,
      numScroll: 1
  }
    
  ] };

  ngOnInit(): void {

    this.products = [
      {
         "image": "d1.jpg",
          "name" :"RANJITH KUMAR THUMPALA",
          "id" :"profileId:VKMG4411",
          "DEMO": "created by:parent"
      },
      {
        "image": "d2.jpg",
        "name" :"SAMPATH KUMAR TARUNI",
        "id" :"profileId:VKMG4408",
        "DEMO": "created by:parent"
     },
     {
      "image": "d3.jpg",
      "name" :"SRI VIDYA PILLA",
      "id" :"profileId:VKMG4404",
      "DEMO": "created by:parent"
   },
   {
    "image": "d4.jpg",
    "name" :"SAI VAMSI GUDIVADA",
    "id" :"profileId:VKMG4403",
    "DEMO": "created by:parent"
 },
 
{
  "image": "d6.jpg",
   "name" :"DIVYA POLINA",
   "id" :"profileId:VKMG4419",
   "DEMO": "created by:parent"
},
{
  "image": "d7.jpg",
   "name" :"KAVYA PADALA",
   "id" :"profileId:VKMG4419",
   "DEMO": "created by:parent"
},
{
  "image": "d16.jpg",
   "name" :"VIJAY KONIDELA",
   "id" :"profileId:VKMG4432",
   "DEMO": "created by:parent"
},
{
  "image": "d8.jpg",
   "name" :"SRILEKHA MESARAPU",
   "id" :"profileId:VKMG4421",
   "DEMO": "created by:parent"
},
{
  "image": "d9.jpg",
   "name" :"LOVAKUMARI MERATLA",
   "id" :"profileId:VKMG4422",
   "DEMO": "created by:parent"
},
{
  "image": "d14.jpg",
   "name" :"HEMANTH KUMAR KANDHIPUDI",
   "id" :"profileId:VKMG4410",
   "DEMO": "created by:parent"
},
{
  "image": "d17.jpg",
   "name" :"SRIHARI NAGISETTY",
   "id" :"profileId:VKMG4433",
   "DEMO": "created by:parent"
},
{
  "image": "d10.jpg",
   "name" :"MEGHANA NITTA",
   "id" :"profileId:VKMG4454",
   "DEMO": "created by:parent"
},
{
  "image": "d18.jpg",
   "name" :"RAJKUMAR CHINTHALA",
   "id" :"profileId:VKMG4457",
   "DEMO": "created by:parent"
},
{
  "image": "d11.jpg",
   "name" :"VANISRI VEMAVARAPU",
   "id" :"profileId:VKMG4439",
   "DEMO": "created by:parent"
},
{
  "image": "d19.jpg",
   "name" :"SRIDHAR MULLAPUDI",
   "id" :"profileId:VKMG4428",
   "DEMO": "created by:parent"

},
{
  "image": "d12.jpg",
   "name" :"CHANDRAKALA PURAM",
   "id" :"profileId:VKMG4467",
   "DEMO": "created by:parent"
},
{
  "image": "d20.jpg",
   "name" :"PUSPHAKUMAR YALAMANCHLI",
   "id" :"profileId:VKMG4498",
   "DEMO": "created by:parent"
},
{
  "image": "d13.jpg",
   "name" :"MOUNIKHA PELAPALA",
   "id" :"profileId:VKMG4490",
   "DEMO": "created by:parent"
},
{
  "image": "d21.jpg",
   "name" :"ROHIT KUMAR MUMMINA",
   "id" :"profileId:VKMG4470",
   "DEMO": "created by:parent"
},
{
  "image": "d5.jpg",
   "name" :"NEHAROY KOPPINA",
   "id" :"profileId:VKMG4465",
   "DEMO": "created by:parent"
},
    ]
  }
  }


