import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http:HttpClient) { }
  userregister(reqdata:any) {
    return this.http.post(`http://localhost:5000/Registration`, reqdata);
  }
}
