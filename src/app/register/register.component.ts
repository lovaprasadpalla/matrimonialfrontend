import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import { throwToolbarMixedModesError } from '@angular/material/toolbar';
import { RegisterService } from './register.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-rigister',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  firstFormGroup = this._formBuilder.group({
    firstCtrl: ['', Validators.required],
  });
  secondFormGroup = this._formBuilder.group({
    secondCtrl: ['', Validators.required],
  });
  
  isLinear = false
  profilefor:any=[{name:"Self"},{name:"Parent"} ,{name:"Sibling"},{name:"Realitive"},{name:"Friends"}]
  mariticalstatus:any=[{name:"Un Married"},{name:"Widow "},{name:"Divorced"},{name:"Saparated"}]
  subcaste:any=[{name:"Blacksmith"},{name:"Carpenters"},{name:"Goldsmith"},{name:"Sculptors"},{name:"Others"}]
  foodtype:any=[{name:"Vegitarian"},{name:"Non Vegitarian"},{name:"Both"}]
  mothertonge:any=[{name:"Telugu"},{name:"English"},{name:"Hindhi"},{name:"Others"}]
  countrylivingin:any=[{name:"india"},{name:"others"}]
  star:any=[{name:"Select"},{name:"Aswini"},{name:"Bharani"},{name:"Kruthika"},{name:"Rohini"},{name:"Mrigasira"},
           {name:"Punarvasu"},{name:"Arudra"},{name:"Pushyami"},{name:"Aslesha"},{name:"Magha"},{name:"Poorva Phalguni"},
           {name:"Uttara Phalguni"},{name:"Chitta"},{name:"Swathi"},{name:"Visakha"},{name:"Anuradha"},{name:"Jesta"},
           {name:"Moola"},{name:"Hastha"},{name:"Poorvashada"},{name:"Uttarashada"},{name:"Sravana"},{name:"Dhanishta"},
           {name:"ravathi"}]
  rasi:any=[{name:"Select"},{name:"Dhanu (Sagittarius)"},{name:"Kanya (Virgo)"},{name:"Karkataka (Cancer)"},
           {name:"Kumbha (Aquarius)"},{name:"Makara (Capricorn)"},{name:"Meena  (Pisces)"},{name:"Mesha (Aries)"},
           {name:"Simha (Leo)"},{name:">Thula  (Libra)"},{name:"Midhuna (Gemini)"},{name:"Vruchika"},{name:"Vrushabha (Taurus)"}]
  height:any=[{name:"122cm - 4ft"},{name:"125cm - 4ft 1in"},{name:"128cm - 4ft 2in"},{name:"134cm - 4ft 4in"},
             {name:"134cm - 4ft 5in"},{name:"137cm - 4ft 6in"},{name:"139cm - 4ft 7in"},{name:"142cm - 4ft 8in"},
             {name:"144cm - 4ft 9in"},{name:"147cm - 4ft 10in"},{name:"149cm - 4ft 11in"},{name:"152cm - 5ft"},
             {name:"154cm - 5ft 1in"},{name:"157cm - 5ft 2in"},{name:"160cm - 5ft 3in"},{name:"162cm - 5ft 4in"},
             {name:"165cm - 5ft 5in"},{name:"167cm - 5ft 6in"},{name:"170cm - 5ft 7in"},{name:"172cm - 5ft 8in"},
             {name:"175cm - 5ft 9in"},{name:"177cm - 5ft 10in"},{name:"180cm - 5ft 11in"},{name:"182cm - 6ft"},
             {name:"185cm - 6ft 1in"},{name:"187cm - 6ft 2in"},{name:"190cm - 6ft 3in"},{name:"193cm - 6ft 4in"},
             {name:"195cm - 6ft 5in"},{name:"198cm - 6ft 6in"},{name:"200cm - 6ft 7in"},{name:"203cm - 6ft 8in"},
             {name:"205cm - 6ft 9in"},{name:"208cm - 6ft 10in"},{name:"210cm - 6ft 11in"},{name:"213cm - 7ft"},
            ]
  cmp:any=[{name:"Select"},{name:"Dark"},{name:"Fair"},{name:"Very Fair"},{name:"Wheatish"},{name:"Wheatish Brown"},
          {name:"Wheatish Medium"},]
  bros:any=[0,1,2,3,4,5,6,7,8,9]
  sis:any=[0,1,2,3,4,5,6,7,8,9]
  mbs:any=[0,1,2,3,4,5,6,7,8,9]
 mss:any=[0,1,2,3,4,5,6,7,8,9]
  ste:any=[{name:"Andrapradesh"},{name:"Others"}]
  country:any=[{name:"india"},{name:"others"}]
  firstname:string="";
  surname:string="";
  ProfileCreatedBy:string="";
  gender:any=[];
  password:string="";
  email:string="";

  mobileno:string="";
  age:string="";
  MaritialStatus:string="";
  Subcaste:string="";
  FoodType:string="";
  Mothertonge:string="";
  Countrylivingin:string="";
  Birth:string="";
  placebirth:string="";
  Star:string="";
  Padam:string="";
  Rasi:string="";
  KujaDosam:string="";
  Gothram:string="";
  Height:string="";
  weight:string="";
  Complexion:string="";
  Education:string="";
  Occupation:string="";
  working:string="";
  salary:string="";
  Ancestralorigin:string="";
  FathersName:string="";
  fatherocc:string="";
  MothersName:string="";
  motherocc:string="";
  Reference:string="";
  Alternatemobile:string="";
  Familydetails:string="";
  Property:string="";
  Partnerpreference:string="";
  Adress:string="";
  City:string="";
  Villege:string="";
  Brothers:string="";
  Sisters:string="";
  Marriedbros:string="";
  Marriedsiss:string="";
  State:string="";
  Country:string="";
  Registerservice:any;
  private _router: any;
  loading = false;
 
    constructor(private _formBuilder: FormBuilder,
       private registerService: RegisterService ,
       private router: Router,
       private toastrService: ToastrService,
      ) { 
    
  }

  ngOnInit(): void {
  }
  changeClient(value:any) {
    console.log(value);
}
changeStatus(value:any) {
  console.log(value);
}
changeCaste(value:any) {
  console.log(value);
}
changeFood(value:any) {
  console.log(value);
}
changeMt(value:any) {
  console.log(value);
}
changeCountry(value:any) {
  console.log(value);
}
changeStar(value:any) {
  console.log(value);
}
changeRasi(value:any) {
  console.log(value);
}
changeHeight(value:any) {
  console.log(value);
}
changeComplexion(value:any) {
  console.log(value);
}
changeBrothers(value:any) {
  console.log(value);
}

changeState(value:any) {
  console.log(value);
}
changeCtry(value:any) {
  console.log(value);
}
changeMarriedbros(value:any) {
  console.log(value);
}
changeSisters(value:any) {
  console.log(value);
}
changeMarriedsiss(value:any) {
  console.log(value);
}

   gen(val:any){
    console.log(this.gender)
    this.gender =val.gender.value
   }
  register(){
    let reqdata = {
      first_name:this.firstname,
      last_name:this.surname,
      created_by:this.ProfileCreatedBy,
      gender:this.gender,
      password:this.password,
      mem_email:this.email,
      // cpsw:this.confirmpassword,
      mobile:this.mobileno,
      dob:this.age,
      marital_status: this.MaritialStatus,
      sub_cast:this.Subcaste,
     foodtype:this.FoodType,
     mother_tongue:this.Mothertonge,
     country_living:this.Countrylivingin,
     mem_tob:this.Birth,
     mem_pob:this.placebirth,
     mem_janmanakshatra:this.Star,
     mem_padam:this.Padam,
     mem_raasi:this.Rasi,
     mem_kujadosham:this.KujaDosam,
     mem_gothram:this.Gothram,
     mem_height:this.Height,
     mem_weight:this.weight,
     mem_complexion:this.Complexion,
     mem_education:this.Education,
     mem_occupation:this.Occupation,
     mem_working:this.working,
     mem_salary:this.salary,
     mem_ancestralorigin:this.Ancestralorigin,
     father_name:this.FathersName,
     father_occuption:this.fatherocc,
     mother_name:this.MothersName,
     mother_occuption:this.motherocc,
     references:this.Reference,
     alternate_mobile:this.Alternatemobile,
     family_details:this.Familydetails,
     property_details:this.Property,
     partner_preferences_Words:this.Partnerpreference,
     address:this.Adress,
     City:this.City,
     Village:this.Villege,
     brothers:this.Brothers,
     sisters:this.Sisters,
     married_brothers:this.Marriedbros,
     married_sisters:this.Marriedsiss,
     State:this.State,
     Country:this.Country,
    }
    console.log(reqdata)
    this.registerService.userregister(reqdata).subscribe((data:any) =>{
      
        this.toastrService.success('Message Success!', 'Title Success!');
        this.router.navigate(["/login"])
      }
    )
  }
       // console.log(data)        
      }
    

function returnUrl(returnUrl: any) {
  throw new Error('Function not implemented.');
}

